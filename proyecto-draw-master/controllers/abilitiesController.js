const express = require('express');

function index(req, res, next) {
  res.send('Muestra todas la habilidades')
}

function create(req, res, next) {
  res.send('Crea una habilidad.')
}

function show(req, res, next) {
  res.send('Muestra una habilidad.')
}

function update(req, res, next) {
  res.send('Edita una habilidad.')
}

function destroy(req, res, next) {
  res.send('Elimina una habilidad.')
}

module.exports = {
  index,
  create,
  show,
  update,
  destroy
}
