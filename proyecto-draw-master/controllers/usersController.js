const express = require('express');

function index(req, res, next) {
  res.send('Muestra todos los usuarios.')
}

function create(req, res, next) {
  res.send('Crea un usuario.')
}

function show(req, res, next) {
  res.send('Muestra un usuario.')
}

function update(req, res, next) {
  res.send('Edita un usuario.')
}

function destroy(req, res, next) {
  res.send('Elimina un usuario.')
}

module.exports = {
  index,
  create,
  show,
  update,
  destroy
}
