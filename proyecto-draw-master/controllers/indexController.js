const express = require('express');

function index(req, res, next) {
  res.send('Index')
}

module.exports = {
  index
}
