const express = require('express');

function index(req, res, next) {
  res.send('Muestra todos los proyectos.')
}

function create(req, res, next) {
  res.send('Crea un proyecto.')
}

function show(req, res, next) {
  res.send('Muestra un proyecto.')
}

function update(req, res, next) {
  res.send('Edita un proyecto.')
}

function destroy(req, res, next) {
  res.send('Elimina un proyecto.')
}

module.exports = {
  index,
  create,
  show,
  update,
  destroy
}
