const http = require("http");

http.createServer(function (require, response){
  // Send th HTTP header
  // HTTP Status: 200 : OK
  // Content Type: text/plain
  response.writeHead(200, {'Content-Type: text/plain'});

  // Send the response body as "Hello world"
  response.end('This is a NodeJS sample application!\n');
}).listen(8080);

//Console.log
